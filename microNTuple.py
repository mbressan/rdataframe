import ROOT
'''ROOT.gInterpreter.Declare(
""" 
#include "funcs.cpp"
""")'''

input_file_name = 'in.root'
output_file_name = 'out.root'

# Open the input ROOT file and read the TTree
tree_name = "PostSel_1e"
d = ROOT.RDataFrame(tree_name, input_file_name)

# Define the new variable(s) for the new tree (perform any desired calculations here)
# d = d.Define('e','el1_pt.size()==1')
# electron_4Vs = 'ConstructP4ID(el1_pt,el1_eta,el1_phi,11)'
# d = d.Define("el1_m", electron_4Vs+".M()") 
new_variable = d.Define("el1_ptx2", "el1_pt*2") 

# Create the output ROOT file and write the new TTree with the derived values
output_file = ROOT.TFile(output_file_name, "RECREATE")
new_tree = new_variable.Define("another_variable", "el1_ptx2 + el1_pt")  # Example: Calculate another variable
new_tree.Snapshot("tree_name", output_file)

# Close the files
output_file.Close()
d.GetFile().Close()
