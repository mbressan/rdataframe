#include <iostream>
#include <cmath>
#include <vector>
#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
using namespace std;

using Vec_t  = const ROOT::VecOps::RVec<float>;
using Track_t  = ROOT::Math::PtEtaPhiEVector; // change name 'track' to something more intuitive?
using Tracks_t = ROOT::VecOps::RVec<Track_t>;
using Track_t_m =  ROOT::Math::PtEtaPhiMVector; // fourvector using m instead of E 
using Tracks_t_m = ROOT::VecOps::RVec<Track_t_m>;

Track_t ConstructP4ID(double pt, double eta, double phi, int pdgId) {
    double mass = 0;
    if (abs(pdgId) == 1) mass = 0.00216;
    if (abs(pdgId) == 2) mass = 0.00467;
    if (abs(pdgId) == 3) mass = 0.093;
    if (abs(pdgId) == 4) mass = 1.27;
    if (abs(pdgId) == 5) mass = 4.18;
    if (abs(pdgId) == 11) mass = 0.000510998946;
    
    ROOT::Math::PtEtaPhiMVector vecm(pt, eta, phi, mass);
    Track_t vec2(vecm.Pt(), vecm.Eta(), vecm.Phi(), vecm.M()); // Use M() for mass instead of E()
    return vec2;
}

Tracks_t_m ConstructP4VectorM(Vec_t& pt, Vec_t& eta, Vec_t& phi, Vec_t& m){
 Tracks_t_m  vec;
 if (pt.size()==0) return vec;
 const auto size = pt.size();
 vec.reserve(size);
 for (auto i = 0; i < pt.size(); ++i) {
  vec.emplace_back(pt,eta,phi,m);
 }
 return vec;
};
